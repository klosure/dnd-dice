
/*  Die implementation file
 *  author: klosure
 *  license: BSD 3-Clause
 */

#include "die.hpp"

/** --- Constructors --- */

Die::Die(u_short s) : sides(s), m_generator(std::random_device()()),
                      m_distribution(1, s) {}


u_short Die::get_sides() {
  return sides;
}


u_short Die::rand_side() {
  return (u_short)m_distribution(m_generator);
}	


Rollee::Rollee(u_short s, u_short n){
  while(n){
    grab(s);
    --n;
  }
}


void Rollee::roll() {
   roll_results.clear();
   for (auto &d : cup_o_dice) {
     roll_results.push_back(d->rand_side());
   }
}


void Rollee::grab(u_short sides) {
  cup_o_dice.push_back(std::make_unique<Die>(sides));
}

void Rollee::print_results(bool interactive) const{
  std::cout << "Results: " << '\n';
  for(auto& r : roll_results){
    std::cout << r << " ";
  }
  std::cout << '\n';
  if(interactive){
    std::cout << "(results printed in the order they were received)" << '\n';
  }
}
