
/*  Main ('Runner') header file
 *  author: klosure
 *  license: BSD 3-Clause
 */

#ifndef MY_MAIN
#define MY_MAIN

#include "die.hpp"

/** --- Non-Member Helper Functions --- **/

void shell(Rollee& rollee_obj) {
  char option;
  std::cout << "dnd-dice - an RPG dice rolling application" << '\n';
  std::cout << "-------------------------------------------" << '\n';
  std::cout << "[Pick a letter to add that die to your cup]" << '\n';
  std::cout << "a) d20" << '\n';
  std::cout << "b) d12" << '\n';
  std::cout << "c) d8" << '\n';
  std::cout << "d) d6" << '\n';
  std::cout << "e) d4" << '\n';
  std::cout << "f) d2" << '\n';
  std::cout << '\n';
  std::cout << "p) percentile" << '\n';
  std::cout << '\n';
  std::cout << "r) Roll!" << '\n';
  std::cout << "q) Quit" << '\n';
  
  std::cin >> option;
  while (option != 'r') {
    switch(option){
    case 'a' :
      rollee_obj.grab(20);
      break;
    case 'b' :
      rollee_obj.grab(12);
      break;
    case 'c' :
      rollee_obj.grab(8);
      break;
    case 'd' :
      rollee_obj.grab(6);
      break;
    case 'e' :
      rollee_obj.grab(4);
      break;
    case 'f' :
      rollee_obj.grab(2);
      break;
    case 'p' :
      rollee_obj.grab(100);
      break;
    case 'q' :
      return;
    }
    std::cin >> option;
  }
  rollee_obj.roll();
  rollee_obj.print_results(true);
}


inline void print_version(const std::string &version) {
  std::cout << "dnd-dice version: " << version << '\n';
}


bool check_args(CLI::App& app, const std::string& v, u_short& s, u_short& n, bool& interactive) {
  if (app.count("-v")) {
    interactive = false;
    print_version(v);
    return false;
  }
  if (app.count("-d")) {
    interactive = false;
    if(s > 100){
      throw "too many sides";
    }
  }
  if (app.count("-n")) {
    interactive = false;
    if (n < 1 || n > 65000) {
      std::cout << n;
      throw "too many dice";
    }
  }
  return true;
}

#endif // MY_MAIN
