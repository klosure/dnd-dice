
/*  Die header file
 *  author: klosure
 *  license: BSD 3-Clause
 */

#ifndef MY_DICE
#define MY_DICE

#include <iostream>
#include <random>
#include <string>
#include <vector>
#include "CLI11.hpp"


typedef unsigned int u_int;
typedef unsigned short u_short;

class Die {
public:
  
  // we default to six sided dice
  Die(u_short sides=6);
  // delete uneeded constructors
  Die(const Die &obj) = delete;
  Die(Die&& obj) = delete;

  // returns how many sides this die has
  u_short get_sides();

  // returns a random number in range 1..maximum_side_val
  u_short rand_side();	

private:
  // how many sides does this die have?
  u_short sides;
  std::mt19937 m_generator;
  std::uniform_int_distribution<int> m_distribution;
};


/**
 *  A Rollee is one who rolls dice of course!
 *  This class handles groups of dice.
 */
class Rollee {
public:

  std::vector<u_short> roll_results;
  
  // defaults to one, six sided die
  Rollee() = default;
  Rollee(u_short s, u_short n);
  Rollee(const Rollee &obj) = delete;
  Rollee(Rollee&& obj) = delete;

  // pick out our Dice objects and put them in our cup
  void grab(u_short sides);
  // Roll dice from the cup
  void roll();
  // print out the result of our roll
  void print_results(bool interactive) const;
  

private:
  // this will hold all of the chosen dice
  std::vector<std::unique_ptr<Die>> cup_o_dice;

};

#endif // MY_DICE
