
/*  Main ('Runner') implementation file
 *  author: klosure
 *  license: BSD 3-Clause
 */

#include "main.hpp"

int main(int argc, char* argv[]) {
  const std::string VERSION = "0.2";
  bool ran_ok = false;

  bool interactive = true;
  u_short sides = 6;
  u_short num_dice = 1;
	
  // parse the user input flags & options
  CLI::App app{ "Roll any number of digital dice for your RPG game" };
  app.ignore_case();

  app.add_flag("-v,--version", "Display the version of dnd-dice");

  app.add_option("-d,--die", sides, "How many sides to the die.");
  app.add_option("-n,--numdice", num_dice, "How many dice to roll.");

  CLI11_PARSE(app, argc, argv);

  bool args_ok = false;
  try {
    args_ok = check_args(app, VERSION, sides, num_dice, interactive);
  }
  catch(...){
    std::cout << "Problem parsing arguments! (Bad input values used?)";	
  }
  if (args_ok) {
    // good input, now construct object
    if(interactive){
      std::unique_ptr<Rollee> sp_rollee_obj_i = std::make_unique<Rollee>();
      shell(*sp_rollee_obj_i);
    }
    else{
      std::unique_ptr<Rollee> sp_rollee_obj = std::make_unique<Rollee>(sides, num_dice);
      sp_rollee_obj->roll();
      sp_rollee_obj->print_results(false);
    }
  }
  
  // exit, returning an int
  if (ran_ok) {
    return 0;
  }
  else {
    return 1;
  }
}
