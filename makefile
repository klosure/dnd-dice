CXX=c++
RM=rm -f
PREFIX=/usr/local
CXXFLAGS=--std=c++14 -pedantic -Wall -Wextra
LDFLAGS = -I include
LDFLAGS += -I tests
LDFLAGS += -I $(PREFIX)/include
LDLIBS=-L$(PREFIX)/lib
BINDIR=$(PREFIX)/bin
MANDIR=$(PREFIX)/share/man/man1

.POSIX: all debug opt

all: dnd-dice

debug: CXXFLAGS += -O0 -g -save-temps -fsanitize=undefined -fsanitize=address -fno-omit-frame-pointer -fno-sanitize-recover
debug: LDFLAGS += -lasan -lubsan
debug: clean dnd-dice

opt: CXXFLAGS += --pipe -O2 -fpie -march=native -fstack-protector-strong
opt: LDFLAGS += -pie -Wl,-z,relro -Wl,-z,now
opt: clean dnd-dice

dnd-dice: die.o main.o
	$(CXX) $(LDFLAGS) -o dnd-dice die.o main.o $(LDLIBS)

die.o: die.cpp
	$(CXX) $(LDFLAGS) $(CXXFLAGS) -c die.cpp

main.o: main.cpp
	$(CXX) $(LDFLAGS) $(CXXFLAGS) -c main.cpp

clean:
	$(RM) *.o *.s *.ii

distclean: clean
	$(RM) *.tar.gz *~ dnd-dice test_dnd-dice

#test: test.cpp
#> run tests

dist: distclean
	tar -czvf ../dnd-dice-source.tar.gz ./
	mv ../dnd-dice-source.tar.gz ./

install:
	cp ./dnd-dice $(BINDIR)
	cp ./dnd-dice.1 $(MANDIR)

uninstall:
	rm $(BINDIR)/dnd-dice
	rm $(MANDIR)/dnd-dice.1
