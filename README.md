![](logo.png)

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

## Motivations:
I just wanna roll some sweet nat 20's! 

## Features
+ Roll d20, d12, d8, d6, d4, d2, and percentile dice!
+ Specify any number of dice!
+ No external dependencies!
+ Portable! Written in pure modern C++14
+ Fully unit test code coverage [coming]

## Prerequisites:
+ Compiler capable of [C++14](https://en.cppreference.com/w/cpp/compiler_support) (*i.e. GCC5* or *Clang3.4*)
+ POSIX [make](https://pubs.opengroup.org/onlinepubs/009695399/utilities/make.html) (*GNU make*, *Bmake*, etc.)

## Install:

#### Source
```bash
git clone https://gitlab.com/klosure/dnd-dice.git
cd dnd-dice
make
make install # as root

# to build a debug version
make debug

# to build a optimized version
make opt
```

#### Binary
Check out the Releases page..

## Usage:

```bash
dnd-dice                       # interactively input dice to roll
dnd-dice -d 20                 # roll a d20
dnd-dice -d 10 -n 4            # roll four d10s
dnd-dice -v                    # show version info
dnd-dice -h                    # show this help info 
```

## Built with:
Free and Open Source software and the help of the church of Emacs ;)

## Notes:
This does not include a secure random number generator. The rolls wont be truly non-deterministic. I use the C++ standard random_device and Mersenne-Twister to get the random numbers. The Mersenne-Twister is considered a really good pseudo-random generator and will produce large volumes of high quality pseudo-random values quickly. This works well enough for this application, and my needs. If you want true randomness build you own application against OpenSSL or just roll real dice.

## License / Disclaimer:
This project is licensed under the BSD 3-Clause license. See LICENSE.md for more info.

Don't blame me when your computer explodes.
